arch-xlicense
=============

lib for handling licences in emacs

This is a derived work of the xlicense lib from Seonk-Kook Shin, but the licenses are chosen after 
the Arch Linux concept of a "common license" (anything under /usr/share/licenses/common).
